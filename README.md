# Prueba técnica - Worky Frontend

En el siguiente documento, te entregamos dos ejercicios que nos permiten evaluar tus habilidades técnicas. Siéntete libre de iniciar con cualquiera de los dos ejercicios.

### Consideraciones previas:  

1. Realiza un *fork* del repositorio.
2. Todos los elementos de las soluciones deben ser accesibles durante la navegación, utiliza enlaces, un menú, una barra lateral, lo que prefieras.
3. Las instrucciones para levantar ambas soluciones deben estar especificadas en un archivo README.md. Lo ideal es que no tengamos que escribirte para resolver dudas o para poder revisar el proyecto.
4. Todos los componentes que utilices deben ser de tu autoría. No debes usar componentes de terceros.
5. Al terminar los ejercicios, sube tu propuesta como un *merge request*.
6. Toda la comunicación que llevaremos será por escrito, y los comentarios que tengamos sobre la prueba los realizaremos directamente sobre tu *merge request*.


###  Ejercicio 1:

#####  Construye un Giphy client que busque perritos

- Utiliza la API de [Giphy](https://developers.giphy.com/).

Crea una Giphy client que permita lo siguiente:
- Realizar búsqueda de GIFs con temática de perritos y desplegar los resultados utilizando la API.
- Permitir guardar el historial de búsqueda.
- Poder marcar como favoritos los GIFs.
- Tanto el historial de búsqueda como los GIFs marcados como favoritos deben ser persistidos de alguna forma, incluso cuando recargo la página. Sé creativo, puedes utilizar el `localStorage` u algún otro medio.

Consideraciones:
- De preferencia, utiliza Vue.js, pero eres libre de elegir con lo que te sientas más cómodo.
- Usa Tailwind CSS para definir los estilos de tus componentes.
- Crea componentes reutilizables.

Opcional:
- Maneja animaciones que ayuden a la experiencia del usuario.


###  Ejercicio 2:

######  Antes de comenzar esta sección, sigue y lee con atención las siguientes instrucciones:

**Consideraciones importantes:** Este ejercicio debe ser construido con Vue 2.x.

1. Ve a https://dummyjson.com/docs/users y lee documentación. Pon atención en los flujos listado, creación y actualización de información.
2. Ve a el siguiente mockup hecho en [Figma](https://www.figma.com/file/BO5iFAlfeairAaNOjZkwpI/Front-End-Test?type=design&node-id=0-1&mode=design&t=hIOHAIIZ1pbSN11w-0).
3. Replica los elementos propuestos en el mockup (Tabla, Modal, Botón, Formularios), recuerda que es importante tu resultado sea lo más cercano al figma.
4. Consume el endpoint que te permite listar los usuarios (`/users`), y muestralos en la tabla que previamente construiste. Mostrando los siguientes Campos "Nombre Completo", "Edad", "Peso" y "Altura".
5. Agrega un botón que permita disparar el flujo de agregar un usuario, Al hacer click al botón se debe visualizar un formulario dentro de un modal con los mismo campos que se muestran en la tabla. (El "Nombre completo" debe ser separado por los elementos que lo componen: Nombre, Apellidos).
6. Agrega una columna a la tabla que muestre un menú desplegable con dos opciones. "Editar" y "Dar de baja". Al presionar editar se mostrará el mismo modal de creación, pero llenado con el usuario que se quiere editar. Al presionar "Dar de baja" consumiras el endpoint que permite eliminar usuarios.
    * Utiliza la referencia visual de figma para el modal y los formularios
    * Estos endpoints simulan una respuesta http `200`, asi que no esperamos que se guarden los datos.
    * Al finalizar ambos flujos la tabla de ser actualizada.

**Puntos extras**
1. Permite la búsqueda por nombre.
2. Implementa un paginador.


¡Mucha suerte!
